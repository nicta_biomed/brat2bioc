package au.com.nicta.csp.bbc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.xml.stream.XMLStreamException;

import au.com.nicta.csp.brateval.Annotations;
import au.com.nicta.csp.brateval.Attribute;
import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;
import au.com.nicta.csp.brateval.Equivalent;
import au.com.nicta.csp.brateval.Event;
import au.com.nicta.csp.brateval.Location;
import au.com.nicta.csp.brateval.Normalization;
import au.com.nicta.csp.brateval.Note;
import au.com.nicta.csp.brateval.Relation;
import bioc.BioCAnnotation;
import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCLocation;
import bioc.BioCNode;
import bioc.BioCPassage;
import bioc.BioCRelation;
import bioc.io.BioCCollectionWriter;
import bioc.io.standard.BioCFactoryImpl;

/**
 * Convert annotation from BRAT format to BioC format
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class BRAT2BioC
{
  public static String readFile (String file_name)
  throws FileNotFoundException
  {
	String text = null;
	Scanner scanner = null; 

	try
	{
	  scanner = new Scanner(new FileInputStream(new File(file_name)));
	  text =  scanner.useDelimiter("\\A").next();
	}
	catch (NoSuchElementException e)
	{ return ""; }
	finally
	{ if (scanner != null) scanner.close(); }

	return text;
  }
  
  private static void addAnnotations(BioCPassage passage, Document d_brat, String file_extension)
  {
	for (Entity e : d_brat.getEntities())
	{
	  BioCAnnotation a = new BioCAnnotation();
	  a.setID(e.getId());
	  a.getInfons().put("type", e.getType());
	  a.getInfons().put("file", e.getFile());
	  
	  List <BioCLocation> list = new LinkedList <BioCLocation> ();
	  
	  for (Location l : e.getLocations())
	  { list.add(new BioCLocation(l.getStart(), l.getEnd()-l.getStart())); }
	  
	  a.setLocations(list);
	  a.setText(e.getString());
	  
	  passage.addAnnotation(a);
	}

	for (Relation r : d_brat.getRelations())
	{
      BioCRelation cr = new BioCRelation();

      cr.setID(r.getId());
      cr.getInfons().put("file", r.getFile());
      cr.getInfons().put("type", "Relation");
      cr.getInfons().put("relation type", r.getRelation());
      cr.getNodes().add(new BioCNode(r.getEntity1().getId(), r.getArg1()));
      cr.getNodes().add(new BioCNode(r.getEntity2().getId(), r.getArg2()));

      passage.addRelation(cr);
    }

	for (Event e : d_brat.getEvents())
	{
      BioCRelation cr = new BioCRelation();

      cr.setID(e.getId());
      cr.getInfons().put("file", e.getFile());
      cr.getInfons().put("type", "Event");
      cr.getInfons().put("event type", e.getType());
      
      cr.getNodes().add(new BioCNode(e.getEventTrigger(), "Trigger"));
      
      for (String arg : e.getArguments())
      {
    	String [] field = arg.split(":");
        cr.getNodes().add(new BioCNode(field[1], field[0]));
      }

      passage.addRelation(cr);
    }
	
	for (Equivalent e : d_brat.getEquivalents())
	{
      BioCRelation cr = new BioCRelation();

      cr.setID("Equiv");
      cr.getInfons().put("type", "Equiv");
      cr.getInfons().put("file", e.getFile());

      for (String equiv : e.getEquivalent())
      { cr.getNodes().add(new BioCNode(equiv, "")); }

      passage.addRelation(cr);
	}

	for (Attribute a : d_brat.getAttributes())
	{
      BioCRelation cr = new BioCRelation();

      cr.setID(a.getId());
      cr.getInfons().put("file", a.getFile());
      cr.getInfons().put("type", "Attribute");
      cr.getInfons().put("attribute type", a.getValue());

      for (String attr : a.getList())
      { cr.getNodes().add(new BioCNode(attr, "")); }

      passage.addRelation(cr);
	}

	for (Normalization n : d_brat.getNormalizations())
	{
	  BioCRelation cr = new BioCRelation();

      cr.setID(n.getId());
      cr.getInfons().put("file", n.getFile());
      cr.getInfons().put("type", "Normalization");
      cr.getInfons().put("normalization type", n.getType());
      cr.getInfons().put("string", n.getString());

      cr.getNodes().add(new BioCNode(n.getEntity(), n.getSourceId()));
	  passage.addRelation(cr);
	}

	for (Note n : d_brat.getNotes())
	{
	  BioCRelation cr = new BioCRelation();

      cr.setID(n.getId());
      cr.getInfons().put("file", n.getFile());
      cr.getInfons().put("type", "Note");
      cr.getInfons().put("note type", n.getType());
      cr.getInfons().put("string", n.getString());

      cr.getNodes().add(new BioCNode(n.getEntity(), ""));
	  passage.addRelation(cr);
	}
  }

  private static BioCCollection loadBRATFiles (String folder_name)
  throws IOException
  {
	BioCCollection c = new BioCCollection();

    File folder = new File(folder_name);

    if (folder.isDirectory() && folder.listFiles() != null)
    {
      for (File file : folder.listFiles())
      {
    	if (file.getName().endsWith(".txt"))
    	{
    	  System.out.println("Processing " + file.getName().replaceAll(".txt$", ""));

    	  // Load text
    	  try
    	  {
    	    String text = readFile(file.getAbsolutePath());

    	    // Look for annotations in different files
    	    Document d_brat = new Document();

    	    boolean ann = true;
    	    boolean a12 = true;

    	    try
    	    { Annotations.read(file.getAbsolutePath().replaceAll(".txt$", ".ann"), "ann", d_brat); }
    	    catch (Exception e)
    	    { ann = false; }

    	    try
    	    {
    	      Annotations.read(file.getAbsolutePath().replaceAll(".txt$", ".a1"), "a1", d_brat);

    	      if(new File(file.getAbsolutePath().replaceAll(".txt$", ".a2")).exists())
    	      { Annotations.read(file.getAbsolutePath().replaceAll(".txt$", ".a2"), "a2", d_brat); }

    	      if(new File(file.getAbsolutePath().replaceAll(".txt$", ".rel")).exists())
    	      { Annotations.read(file.getAbsolutePath().replaceAll(".txt$", ".rel"), "rel", d_brat); }
    	    }
    	    catch (Exception e)
    	    { a12 = false; }

    	    if (ann || a12)
    	    {
    	      // Add document to collection
    	      BioCDocument document = new BioCDocument();
    	      c.addDocument(document);

    	      // Set the id to the name of the file
    	      document.setID(file.getName().replaceAll(".txt$", ""));

    	      BioCPassage passage = new BioCPassage();

    	      passage.setOffset(0);
    	      passage.setText(text);

    	      addAnnotations(passage, d_brat, "ann");

    	      document.addPassage(passage);
      	      System.out.println("Done " + file.getName().replaceAll(".txt$", ""));
    	    }
    	    else
    	    {
    	  	  System.out.println("Done " + file.getName().replaceAll(".txt$", ""));
    	      System.out.println("No annotations for " + file.getName().replaceAll(".txt$", ""));
    	    }
    	  }
    	  catch (Exception e)
    	  {
    		System.out.println("Error processing file " + file.getName().replaceAll(".txt$", ""));
    		e.printStackTrace(System.out);
    	  }
    	}
      }
    }
    else
    { throw new IOException(folder_name + " is not a directory or it is empty."); }

    return c;
  }

  public static void main (String [] argc)
  throws IOException, XMLStreamException
  {
	if (argc.length != 2)
	{
      System.err.println("BRAT2BioC input_BRAT_folder_name output_BioC_file_name");
      System.exit(-1);
	}

	// Load BRAT documents into the BioC data structure
	BioCCollection c = loadBRATFiles(argc[0]);
    // Serialize the BioC collection
	BioCFactoryImpl f = new BioCFactoryImpl();

	BioCCollectionWriter w = f.createBioCCollectionWriter(new BufferedWriter(new FileWriter(argc[1])));
	w.writeCollection(c);
	w.close();
  }
}