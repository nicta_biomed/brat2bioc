package au.com.nicta.csp.bbc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import au.com.nicta.csp.brateval.Annotations;
import au.com.nicta.csp.brateval.Attribute;
import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;
import au.com.nicta.csp.brateval.Equivalent;
import au.com.nicta.csp.brateval.Event;
import au.com.nicta.csp.brateval.Location;
import au.com.nicta.csp.brateval.Normalization;
import au.com.nicta.csp.brateval.Note;
import au.com.nicta.csp.brateval.Relation;
import bioc.BioCAnnotation;
import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCLocation;
import bioc.BioCNode;
import bioc.BioCPassage;
import bioc.BioCRelation;
import bioc.io.BioCCollectionReader;
import bioc.io.standard.BioCFactoryImpl;

public class BioC2BRAT
{
  private static void writeBRATFiles(BioCCollection c, String folder_name)
  throws IOException
  {
    for (BioCDocument d : c.getDocuments())
    {

      for (BioCPassage p : d.getPassages())
      {
    	int passage_id = 1;

    	// Map entity ids to valid brat entity ids
    	Map <String, Entity> entity_mapping =
    			new HashMap <String, Entity> ();

        Map <String, Document> map_doc =
        		new HashMap <String, Document> ();

        for (BioCAnnotation a : p.getAnnotations())
        {
          String id = a.getID();

          // Modify this to adopt entities which occupy several spans
          LinkedList <Location> l = new LinkedList <Location> ();

          for (BioCLocation bcl : a.getLocations())
          {	l.add(new Location(bcl.getOffset(), bcl.getOffset() + bcl.getLength())); }

          Entity e = new Entity(id, a.getInfon("type"), l, a.getText(), a.getInfon("file"));

          Document bd = map_doc.get(a.getInfon("file"));

          if (bd == null)
          {
        	bd = new Document();
        	map_doc.put(a.getInfon("file"), bd);
          }

          // Id can be a problem. Relations depend on it. Create a mapping table
          bd.addEntity(a.getID(), e);

          entity_mapping.put(a.getID(), e);
        }

        for (BioCRelation r : p.getRelations())
        {
          String type = r.getInfon("type");

          if (type.equals("Relation"))
          {
            String id = r.getID();

            Relation br = new Relation(id, r.getInfon("relation type"), r.getNodes().get(0).getRole(), entity_mapping.get(r.getNodes().get(0).getRefid()), r.getNodes().get(1).getRole(), entity_mapping.get(r.getNodes().get(1).getRefid()), r.getInfon("file"));

            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
        	  bd = new Document();
        	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addRelation(id, br);
          }
          else if (type.equals("Equiv"))
          {    	
        	LinkedList <String> es = new LinkedList <String> ();
        	  
          	for (BioCNode n : r.getNodes())
          	{ es.add(n.getRefid()); }
          	
          	Equivalent e = new Equivalent(es, r.getInfon("file"));
          	
            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
        	  bd = new Document();
        	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addEquivalent(e);
          }
          else if (type.equals("Event"))
          {
        	String id = r.getID();

        	String t_trigger = "";
        	
        	LinkedList <String> arguments = new LinkedList <String> ();

        	for (BioCNode n : r.getNodes())
        	{
        	  if (n.getRole().equals("Trigger"))
        	  { t_trigger = n.getRefid(); } 
        	  else
        	  { arguments.add(n.getRole() + ":" + n.getRefid()); }
        	}

        	Event e = new Event(id, r.getInfon("event type"), t_trigger, arguments, r.getInfon("file"));

            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
        	  bd = new Document();
        	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addEvent(id, e);        	
          }
          else if (type.equals("Attribute"))
          {
            String id = r.getID();
            String value = r.getInfon("attribute type");

            LinkedList <String> list = new LinkedList <String> ();

        	for (BioCNode n : r.getNodes())
        	{ list.add(n.getRefid()); }

            Attribute a = new Attribute(id, value, list, r.getInfon("file"));

            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
        	  bd = new Document();
        	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addAttribute(id, a);        	
          }
          else if (type.equals("Normalization"))
          {
            String id = r.getID();
            String string = r.getInfon("string");
            String n_type = r.getInfon("normalization type");
            String source_id = r.getNodes().get(0).getRefid();
            String entity = r.getNodes().get(0).getRole();

            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
          	  bd = new Document();
          	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addNormalization(id, new Normalization(id, n_type, entity, source_id, string, r.getInfon("file")));        	
          }
          else if (type.equals("Note"))
          {
            String id = r.getID();
            String string = r.getInfon("string");
            String n_type = r.getInfon("note type");
            String entity = r.getNodes().get(0).getRefid();

            Document bd = map_doc.get(r.getInfon("file"));

            if (bd == null)
            {
          	  bd = new Document();
          	  map_doc.put(r.getInfon("file"), bd);
            }

            bd.addNote(id, new Note(id, n_type, entity, string, r.getInfon("file")));        	
          }
        }

        // If only one passage, the document id is used as seed name + extention.
        // Other wise an id is added to the name
        String text_file_name;
        String annotation_file_name;

        if (d.getPassages().size() == 1)
        {
          text_file_name = d.getID() + ".txt";
          annotation_file_name = d.getID() + ".";
        }
        else
        {
          text_file_name = d.getID() + ".txt";
          annotation_file_name = d.getID() + "_" + passage_id + ".";
        }

        BufferedWriter w = new BufferedWriter(new FileWriter(new File (folder_name, text_file_name)));
        w.write(p.getText());
        w.close();

        for (Map.Entry <String, Document> doc : map_doc.entrySet())
        { Annotations.write(new File (folder_name, annotation_file_name + doc.getKey()).getAbsolutePath(), doc.getValue()); }
      }
    }
  }

  public static void main (String [] argc)
  throws XMLStreamException, IOException
  {
	if (argc.length != 2)
	{
      System.err.println("BRAT2BioC input_BioC_file_name output_BRAT_folder_name");
      System.exit(-1);
	}

    BioCFactoryImpl f = new BioCFactoryImpl();

	// Load BioC model in memory
	BioCCollectionReader r = f.createBioCCollectionReader(new BufferedReader(new FileReader(argc[0])));
	BioCCollection c = r.readCollection();
	r.close();

	writeBRATFiles(c, argc[1]);
  }
}